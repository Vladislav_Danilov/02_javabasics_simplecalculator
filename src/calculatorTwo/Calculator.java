package calculatorTwo;

import java.io.*;
import java.util.*;

/**
 * 
 * This class converts and finds expression on Polish Notation
 * 
 * @author Vladislav_Danilov
 *
 */
public class Calculator {

  private final static char PLUS = '+';
  private final static char MINUS = '-';
  private final static char DIVIDE = '/';
  private final static char MULTI = '*';
  private final static char RIGHTBRACKET = '(';
  private final static char LEFTBRACKET = ')';
  private BufferedReader streamFromTheConsole;
  private char token;


  /**
   * The class constructor
   * 
   * @param streamFromTheConsole
   */
  public Calculator(BufferedReader streamFromTheConsole) {
    this.streamFromTheConsole = streamFromTheConsole;
  }

  /**
   * Converts expression in Polish notation
   * 
   * @return polish notation string
   * @throws CalculateException
   */
  public String reversePolishNotation() throws CalculateException {
    StringBuilder concentratorTemp = new StringBuilder("");
    StringBuilder concentratorDigite = new StringBuilder("");
    char cTmp;

    while (token != '\r') {
      try {
        token = (char) streamFromTheConsole.read();
      } catch (IOException e) {
        e.printStackTrace();
      }
      if (operatorChecking(token)) {
        while (concentratorTemp.length() > 0) {
          cTmp = concentratorTemp.substring(concentratorTemp.length() - 1).charAt(0);
          if (operatorChecking(cTmp) && (priorityOperation(token) <= priorityOperation(cTmp))) {
            concentratorDigite.append(" ").append(cTmp).append(" ");
            concentratorTemp.setLength(concentratorTemp.length() - 1);
          } else {
            concentratorDigite.append(" ");
            break;
          }
        }
        concentratorDigite.append(" ");
        concentratorTemp.append(token);
      } else if (RIGHTBRACKET == token) {
        concentratorTemp.append(token);
      } else if (LEFTBRACKET == token) {
        cTmp = concentratorTemp.substring(concentratorTemp.length() - 1).charAt(0);
        while (RIGHTBRACKET  != cTmp) {
          if (concentratorTemp.length() < 1) {
            throw new CalculateException("Error brackets.");
          }
          concentratorDigite.append(" ").append(cTmp);
          concentratorTemp.setLength(concentratorTemp.length() - 1);
          cTmp = concentratorTemp.substring(concentratorTemp.length() - 1).charAt(0);
        }
        concentratorTemp.setLength(concentratorTemp.length() - 1);
      } else {
        // ���� ������ �� �������� - ��������� � �������� ������������������
        concentratorDigite.append(token);
      }
    }

    // ���� � ����� �������� ���������, ��������� �� � ������� ������
    while (concentratorTemp.length() > 0) {
      concentratorDigite.append(" ")
          .append(concentratorTemp.substring(concentratorTemp.length() - 1));
      concentratorTemp.setLength(concentratorTemp.length() - 1);
    }
    return concentratorDigite.toString();
  }


  /**
   * Checks authorized operators
   * 
   * @param tokenOperator
   * @return true operator has, false no operator
   */
  public boolean operatorChecking(char tokenOperator) {
    switch (tokenOperator) {
      case MINUS:
      case PLUS:
      case MULTI:
      case DIVIDE:
        return true;

    }
    return false;
  }

  /**
   * Returns priority operation
   * 
   * @param operation char
   * @return byte
   */
  public byte priorityOperation(char operation) {
    switch (operation) {
      case MULTI:
      case DIVIDE:
        return 2;
    }
    return 1; // ��� �������� + � -
  }

  /**
   * Counts of expression in Reverse Polish Notation
   * 
   * @param stringPolishNotation input string Polish Notation
   * @return double result express decision
   */
  public double calculate(String stringPolishNotation) throws CalculateException {
    double currentVariable = 0;
    double nextVariable = 0;
    String tempString;
    Deque<Double> stack = new ArrayDeque<Double>();
    StringTokenizer st = new StringTokenizer(stringPolishNotation);
    while (st.hasMoreTokens()) {
      try {
        tempString = st.nextToken().trim();
        if (1 == tempString.length() && operatorChecking(tempString.charAt(0))) {
          if (stack.size() < 2) {
            throw new CalculateException(
                "Incorrect amount of data on the stack for the operation " + tempString);
          }
          nextVariable = stack.pop();
          currentVariable = stack.pop();
          switch (tempString.charAt(0)) {
            case PLUS:
              currentVariable += nextVariable;
              break;
            case MINUS:
              currentVariable -= nextVariable;
              break;
            case DIVIDE:
              currentVariable /= nextVariable;
              break;
            case MULTI:
              currentVariable *= nextVariable;
              break;
            default:
              throw new CalculateException("Illegal operation " + tempString);
          }
          stack.push(currentVariable);
        } else {
          currentVariable = Double.parseDouble(tempString);
          stack.push(currentVariable);
        }
      } catch (CalculateException e) {
        e.printStackTrace();
      }
    }

    if (stack.size() > 1) {
      throw new CalculateException(
          "The number of operators does not correspond to the number of operands");
    }

    return stack.pop();
  }
}
